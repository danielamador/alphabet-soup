/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alphabetsoup;

/**
 *
 * @author Daniel
 */

import java.io.File; 
import java.io.FileNotFoundException; 
import java.util.Scanner;

public class AlphabetSoup {
    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        String line;
        String filePath = "";
        WordSearchFinder wordFinder = new WordSearchFinder();
        if (args.length == 0){
            System.out.println("Please Enter FilePath: ");
            while ((line = scanner.nextLine()).length() > 0 && line != null ){
                System.out.println("Entry: " + line);
                File tempFile = new File(line);
                if (!tempFile.exists() || tempFile.isDirectory()){
                    System.out.println("Invalid path. Please try again: ");
                } else {
                    filePath = line;
                    break;
                }
            }
            wordFinder.searchAllWithFile(filePath);
        }
        
        for (String nPath: args){
            wordFinder.searchAllWithFile(nPath);
        }       
    }
}

