/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alphabetsoup;

import java.util.Arrays;

/**
 *
 * @author Daniel
 */
public class WordSearchChecklist {
    
    private WordSearchChecklistItem[] searchResults;
    private int maxX;
    private int maxY;    
    
    public void resetContentsWith(String [] input, int maxY, int maxX){//create a new checklist, discarding the old one
        if (maxX < 0 || maxY < 0) {
            System.out.println("Invalid max coordinate values");
            return;
        } else if (input.length < 1){
            System.out.println("Invalid Entry: Empty Wordlist");
            return;
        }
        int size = input.length;
        searchResults = new WordSearchChecklistItem[size];

        int count = 0;
        for (String n: input){
            searchResults[count] = new WordSearchChecklistItem(n); //crosswordData[].name = input.name
            count++;
        }
        this.maxX = maxX; //x and y dimensions of the board
        this.maxY = maxY;
    }
        
    public void foundWord(String inputWord, int x1, int y1, int x2, int y2){
        String[]tempList = new String[searchResults.length];
        int count = 0;
        for (WordSearchChecklistItem n: searchResults){
            tempList[count] = n.getWord();
            count++;
        }
        if ( !Arrays.asList(tempList).contains(inputWord) ){
            System.out.println("Invalid crossoff entry: Word not found");
            return;
        } else if (x1 < 0 || x1 < 0 || y1 < 0 || y2 < 0) {
            System.out.println("Invalid Coordinates: Less than Zero");
            return;
        } else if (x1 > maxX || x2 > maxX || y1 > maxY || y2 > maxY) {
            System.out.println("Invalid coordinates: Outside of grid size");
            return;
        }
        int selectedWordIndex = Arrays.asList(tempList).indexOf(inputWord);
        this.searchResults[selectedWordIndex].setStartCoordinates(x1, y1);
        this.searchResults[selectedWordIndex].setEndCoordinates(x2, y2);
        this.searchResults[selectedWordIndex].setIsFound(true);
    }
    public boolean haveAllBeenFound(){
        for (WordSearchChecklistItem n: searchResults){
            if (n.getIsFound() == false)
                return false;
        }
        return true;
    }
    public String[] getWordsNotFound(){
        String [] returnList = new String[this.numNotFound()];
        int returnListCount = 0;
        for (WordSearchChecklistItem n: searchResults) {
            if (n.getIsFound() == false){
                returnList[returnListCount] = n.getWord();
                returnListCount++;
            }
        }
        return returnList;
    }
    private int numNotFound(){
        int count = 0;
        for (WordSearchChecklistItem n: searchResults){
            if (n.getIsFound() == false)
                count++;
        }
        return count;
    }
    public int getLengthOfShortestNotFound(){
       
        int minLength = maxY < maxX ? maxX : maxY;//assigned the dimensions of the board as defaults
        String [] temp = getWordsNotFound();
        for (String n: temp){
            if (n.length() < minLength)
                minLength = n.length();
        }
        return minLength;
    }
    public String getWordsFoundAsString(){
        String results = "";
        for (WordSearchChecklistItem n: searchResults){
            if (n.getIsFound() == false) {
                continue;
            }
            results += n.getWord() + " " + n.getStartCoordinatesAsString() + " " + n.getEndCoordinatesAsString() + "\n";
        }
        return results;
    }
    public String getWordsNotFoundAsString() {
        String[] temp = this.getWordsNotFound();
        String results = "";
        for (String n: temp){
            results += n + " ";
        }
        return results;
    }    
}