/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alphabetsoup;

/**
 * Character grid with coordinates embedded
 * @author Daniel
 */
public class CharGrid {
    private char letter;
    private int [] coordinates = new int[2];
    
    CharGrid(char letter){
        this.letter = letter;
    }
    
    public void setLetter(char letter){
        this.letter = letter;
    }
    
    public char getLetter(){
        return this.letter;
    }
    
    public void setCoordinates(int y, int x){
        this.coordinates[0] = y;
        this.coordinates[1] = x;
    }
    
    public int[] getCoordinates(){
        return this.coordinates;
    }
    
    public String getCoordinatesAsString(){
        String temp = "(" + this.coordinates[0] + "," + this.coordinates[1] + ")";
        return temp;
    }
}
