/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alphabetsoup;

/**
 *
 * @author Daniel
 */
public class WordSearchChecklistItem {
    private String word;
    private int [] endCoordinates = new int[2];
    private int [] startCoordinates = new int[2];
    private boolean isFound = false;

    WordSearchChecklistItem(String inputWord){
        if (inputWord.length() < 1){
            System.out.println("Invalid word: Length too short");
            return;
        }
        this.word = inputWord;
    }
    
    public String getStartCoordinatesAsString(){
        return "(" + startCoordinates[0] + "," + startCoordinates[1] + ")";
    }
    
    public String getEndCoordinatesAsString(){
        return "(" + endCoordinates[0] + "," + endCoordinates[1] + ")";
    }
    
    public String getWord(){
            return word;
    }
    
    public boolean getIsFound(){
        return isFound;
    }
    public void setStartCoordinates(int x1, int y1){
        this.startCoordinates[0] = x1;
        this.startCoordinates[1] = y1;
    }
    public void setEndCoordinates(int x2, int y2){
        this.endCoordinates[0] = x2;
        this.endCoordinates[1] = y2;
    }

    public void setWord(String word){
        if (word.length() < 1)
            return;
        this.word = word;
    }

    public void setIsFound(boolean isFound){
        this.isFound = isFound;
    }
}
