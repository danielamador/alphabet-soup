/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alphabetsoup;

/**
 *
 * @author Daniel
 */

import java.io.File; 
import java.io.FileNotFoundException; 
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WordSearchFinder {
    private int xDimensions;
    private int yDimensions;
    private WordSearchChecklist searchData = new WordSearchChecklist();
    
    private CharGrid[][] grid;
    private String[] wordList;
    private String filePath;
    private int maxSideLength;
    
    public void searchAllWithFile(String inputPath){
        filePath = inputPath;
        File file = new File(filePath);
        if (!file.exists() || file.isDirectory()){
            System.out.println("Invalid path entry: \"" + inputPath + "\" \nPlease try again");
            return;
        }
        resetFileToSearch(inputPath);
        runParse();
        printResults();
    }
    
    private CharGrid[][] populateGridFromLines(Scanner gridScanner){
        CharGrid[][]tempGrid = new CharGrid[yDimensions][xDimensions];
        int countY = 0;
        while (countY < yDimensions){
            String holder = gridScanner.nextLine();
            holder = holder.replaceAll("[\\s,\\[\\]]", "");
            int countX = 0;
            while (countX < xDimensions){
                tempGrid[countY][countX] = new CharGrid(holder.charAt(countX));//grid[][].letter = charVal
                tempGrid[countY][countX].setCoordinates(countY,countX);
                countX++;
            }
            countY++;
        }
        return tempGrid;
    }
    
    private int getRemainingScannerLines(Scanner inputScanner){
        int remainingLines = 0;
        while (inputScanner.hasNext()){ 
            inputScanner.nextLine();
            remainingLines++;
        }
        return remainingLines;
    }
    
    private Scanner skipAheadInScanner(int numPaces, Scanner inputScanner){
        int count = 0;
        while (count < numPaces){
            inputScanner.nextLine();
            count++;
        }
        return inputScanner;
    }
    
    private String[] extractWordListFromScanner(Scanner wordListScanner, int numLines){
        String[] wordList = new String [numLines]; 
        int wordCount = 0;
        while ( wordListScanner.hasNext() ) {
            wordList[wordCount] = wordListScanner.nextLine().replaceAll("[\\s,]", "");
            wordCount++;
        }
        return wordList;
    }
    
    private String [] getXYDimensionsFromLine(Scanner gridScanner){
        String temp = gridScanner.nextLine();
        String[] result = temp.split("x");
        return result;
    }
    
    private void resetFileToSearch(String inputPath){
        this.filePath = inputPath;
        File file = new File(filePath);
        if (!file.exists() || file.isDirectory()){
            System.out.println("Invalid path. Please try again: ");
            return;
        }
        Scanner gridScanner, wordListScanner;
        try {
            gridScanner = new Scanner(file);
            wordListScanner = new Scanner(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WordSearchFinder.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        String[] tempDimensions = getXYDimensionsFromLine(gridScanner);
        this.yDimensions = Integer.parseInt(tempDimensions[0]);
        this.xDimensions = Integer.parseInt(tempDimensions[1]);
        this.maxSideLength = this.xDimensions > this.yDimensions ? this.xDimensions : this.yDimensions;
        this.grid = populateGridFromLines(gridScanner);
        int remainingLines = getRemainingScannerLines(gridScanner);
        wordListScanner = skipAheadInScanner(xDimensions + 1, wordListScanner);
        this.wordList = extractWordListFromScanner(wordListScanner, remainingLines);
        this.searchData.resetContentsWith(this.wordList, this.yDimensions, this.xDimensions);
    }
    
    private void runParse(){
        parseGridSearch(grid, wordList);//check rows
        CharGrid[][] tempGrid = rotateUprightCharGrid(grid);
        wordList = searchData.getWordsNotFound();
        parseGridSearch(tempGrid, wordList);//check columns
        tempGrid = rotateDiagonalCharGrid(grid, true);
        parseGridSearch(tempGrid, wordList);//check diagonal left -> right
        CharGrid[][] temp2 = rotateDiagonalCharGrid(grid, false);
        parseGridSearch(temp2, wordList);//check diagonal right -> left
    }
    
    private void parseGridSearch(CharGrid[][]inputGrid, String[] wordsNotFound){
        if (wordsNotFound.length == 0)
            return;
        String tempRow = "";
        int countY = 0;
        while (countY < inputGrid.length) {
            tempRow = getStringFromCharRow(inputGrid[countY]);
            for (String n: wordsNotFound){
                String reverseN = new StringBuilder(n).reverse().toString();
                if (tempRow.contains(n)) {
                    saveFoundInfo(tempRow, inputGrid, countY, n, false);
                } else if (tempRow.contains(reverseN)){
                    saveFoundInfo(tempRow, inputGrid, countY, reverseN, true);
                }
            }
            countY++;
        }
    }
    
    private String getStringFromCharRow (CharGrid[] inputCharRow) {
        String result = "";
        for (CharGrid n: inputCharRow){
            result += n.getLetter();
        }
        return result;
    }    
    
    private void printGrid(CharGrid input[][]){
        for (CharGrid[] row: input){
            for (CharGrid n: row){
                System.out.print(n.getLetter() + " " + n.getCoordinatesAsString());
            }
            System.out.println();
        }
    }
    
    private CharGrid[][] rotateUprightCharGrid(CharGrid[][] inputGrid){//rotate rows so columns can be processed as rows
        CharGrid[][] tempGrid = new CharGrid[inputGrid.length][maxSideLength];
        int countY = 0;
        while (countY < inputGrid.length){
            int countX = 0;
            for(CharGrid n: inputGrid[countY]){
                tempGrid[countX][countY] = new CharGrid(n.getLetter());
                countX++;
            }
            countY++;
        }
        return tempGrid;
    }
    
    private CharGrid[][] rotateDiagonalCharGrid(CharGrid[][] inputGrid, boolean clockwise){
        CharGrid[][] tempGrid = new CharGrid[yDimensions + xDimensions - 1][];
        int startingY = yDimensions - 1;
        int startingX = clockwise ? 0 : xDimensions - 1;
        int outGridY = 0;
        while (clockwise ? startingX < xDimensions : startingX >= 0){
            tempGrid[outGridY] = getDiagonalWord(inputGrid, startingY, startingX, clockwise);
            int yx [] = shiftStartingCoordinates( startingY, startingX, clockwise);
            startingY = yx[0];
            startingX = yx[1];
            outGridY++;
        }
        return tempGrid;
    }
    
    private CharGrid[] getDiagonalWord(CharGrid[][] inputGrid, int startingY, int startingX, boolean clockwise){
        if (startingY < 0 || startingX < 0 || startingY > yDimensions || startingX > xDimensions){
            System.out.println("invalid coordinate entries, setting to (0,0)");
            startingX = 0;
            startingY = 0;
        }      
        int size = calculateDiagonalLength(startingY, startingX, clockwise);
        CharGrid[] tempGrid = new CharGrid[size]; 
        int y = startingY;
        int x = startingX;
        int count = 0;
        while (count < size && y < yDimensions && (clockwise ? x < xDimensions : x >= 0)){
            tempGrid[count] = new CharGrid(inputGrid[y][x].getLetter());
            int[]coordinates = inputGrid[y][x].getCoordinates();
            tempGrid[count].setCoordinates(coordinates[0], coordinates[1]);
            y++;
            x = clockwise ? x + 1 : x - 1;
            count++;
        }
        return tempGrid;
    }
    
    private void saveFoundInfo(String tempRow, CharGrid[][]inputGrid, int countY, String n, boolean isReverse){
        int index = tempRow.indexOf(n);
        int lastIndex = index + n.length() - 1;
        int []tempStartCoordinates = inputGrid[countY][index].getCoordinates();
        int []tempEndCoordinates = inputGrid[countY][lastIndex].getCoordinates();
        if (isReverse){
            searchData.foundWord(new StringBuilder(n).reverse().toString(), tempStartCoordinates[0], tempStartCoordinates[1],
                tempEndCoordinates[0], tempEndCoordinates[1]);
        } else {
            searchData.foundWord(n, tempStartCoordinates[0], tempStartCoordinates[1],
                tempEndCoordinates[0], tempEndCoordinates[1]);
        }
    }
    
    /**
    * Shifts starting coordinates to be along left and top border or right and top border
    */
    private int[] shiftStartingCoordinates(int startingY, int startingX, boolean clockwise){
        int [] yx = new int [] {startingY, startingX};
        if (yx[0] > 0){
            yx[0] = yx[0] - 1;
        } else if (clockwise == true){
            yx[1] = yx[1] + 1;
        } else {
            yx[1] = yx[1] - 1;
        }
        return yx;
    }
    
    private void printResults(){
        System.out.println(searchData.getWordsFoundAsString());
        if (searchData.haveAllBeenFound() == false){
            System.out.println("Sorry, the following were not found: \n" + searchData.getWordsNotFoundAsString());
        }
    }
    
    private int calculateDiagonalLength(int startingY, int startingX, boolean clockwise){
        int y = startingY;
        int x = startingX;
        int count = 0;
        while ( y < yDimensions && x < xDimensions && x > -1){
            y++;
            x = clockwise ? x + 1 : x - 1;
            count++;
        }
        return count;
    }
}
